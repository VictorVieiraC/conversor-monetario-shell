#!/bin/bash
set -e

moeda_base=$1
moeda_conv=$2
valor=$3

if [ $# -ne 3 ]; then
   echo "[ERRO] Informe todos os parametros corretamente. Ex: USD EUR 100"
   exit 1
fi

multiplicador=`curl 'https://api.exchangeratesapi.io/latest?base='${moeda_base}'&symbols='${moeda_conv} | jq '.rates.'"${moeda_conv}"`

python -c "print ${multiplicador} * ${valor}"

